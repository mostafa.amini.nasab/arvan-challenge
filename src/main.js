import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import BlogSingle from './routes/BlogSingle.vue';
import Blog from './routes/Blog.vue';
import Login from './routes/Login.vue';
import Home from './routes/Home';
import About from './routes/About';
import Contact from './routes/Contact';
import Register from './routes/Register';
import AC_Authentication from './lib/authentication';

Vue.use(VueRouter);

Vue.config.productionTip = false;

// Get User's login state before mounting the application
// Create single AC_Authentication instance and share it between routes to keep login state in a single place
let auth = new AC_Authentication();

auth.getCurrentUser()
.finally(() => {
  const ROUTES = [
    {
      path: '/',
      component: Home
    },
    {
      name: 'blog_single',
      path: '/blog_single/:slug',
      component: BlogSingle
    },
    {
      path: '/blog',
      component: Blog
    },
    {
      path: '/login',
      component: Login,
      beforeEnter: function(to, from, next){
        if(auth.currentUser){
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: '/register',
      component: Register,
      beforeEnter: function(to, from, next){
        if(auth.currentUser){
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: '/about',
      component: About
    },
    {
      path: '/contact',
      component: Contact
    },
  ];

  ROUTES.forEach((route) => {
    route['props'] = { auth };
  });
  
  const ROUTER = new VueRouter({
    routes: ROUTES
  });
  
  new Vue({
    router: ROUTER,
    render: h => h(App),
  }).$mount('#app')  
});