import AC_Storage from './storage';
import RealWorldApi from './api';
import config from './config';

const TOKEN_KEY = 'ac_token';

/**
 * Class to do authentication related tasks like login, register, etc. users RealWorlApi class.
 */
class AC_Authentication {
    constructor(){
        let { apiUrl, endpoints } = config;
        this.storage = new AC_Storage();
        this.api = new RealWorldApi(apiUrl);
        if(endpoints){
            this.loginEndpoint = endpoints.login;
            this.registerEndpoint = endpoints.register;
        }
    }

    /**
     * gets the stored token. uses AC_Storage class which abstracts the storage details.
     */
    getToken(){
        return this.storage.get(TOKEN_KEY);
    }

    /**
     * sets the stored token. uses AC_Storage class which abstracts the storage details.
     * 
     * @param {string} token - jwtToken received from a successfull login
     */
    setToken(token){
        this.storage.set(TOKEN_KEY, token);
    }

    removeToken(){
        this.storage.remove(TOKEN_KEY);
    }

    /**
     * return a promise which will be resolved to logged in user if the login was successfull. else the promise will be rejected with the returned errors.
     * 
     * @param {*} user - should be an object with email and password properties. e.g. { 'email': 'mostafa.amini.nasab@gmail.com', 'password': '123456789' }
     */
    login(user){
        let loginEndpoint = this.loginEndpoint;

        if(!user){
            throw new Error('invalid user');
        }

        let { email, password } = user;

        let loginData = {
            'user': {
                'email': email,
                'password': password
            }
        }

        let token = this.getToken();

        let loginPromise = this.api.post(loginEndpoint, loginData, token).then((response) => {
            let status = response.status;
            return response.json().then((result) => {
                if(status == 200){
                    if(result && result.user){
                        let user = result.user;
                        let { token } = user;
                        this.setToken(token);
                        this.currentUser = user;
                        return user;
                    }
                } else if(status == 422){
                    if(result && result.errors){
                        let errors = result.errors || {};
                        throw errors;
                    }
                }
                throw response;
            });
        });

        return loginPromise;   
    }

    register(user){
        let registerEndpoint = this.registerEndpoint;

        if(!user){
            throw new Error('invalid user');
        }

        let { username, email, password } = user;

        let registerData = {
            'user': {
                'username': username,
                'email': email,
                'password': password
            }
        }

        let registerPromise = this.api.post(registerEndpoint, registerData).then((response) => {
            let status = response.status;
            return response.json().then((result) => {
                if(status == 200){
                    if(result && result.user){
                        let user = result.user;
                        let { token } = user;
                        this.setToken(token);
                        this.currentUser = user;
                        return user;
                    }
                } else if(status == 422){
                    if(result && result.errors){
                        let errors = result.errors || {};
                        throw errors;
                    }
                }
                throw response;
            });
        });

        return registerPromise; 
    }

    /**
     * return a promise to get the current logged in user. if user is not logged in, the promise will be resolved to 'null'.
     */
    getCurrentUser(){
        let token = this.getToken();
        let getUserPromise = this.api.get('user', token)
        .then((response) => {
            if(response.ok){
                return response.json().then((result) => {
                    this.currentUser = (result && result.user);
                    return this.currentUser;
                })
                .catch(() => {
                    return null;
                });
            }
            return null;
        })
        .catch(() => {
            return null;
        });
        
        return getUserPromise;
    }

    /**
     * since there is no logout endpoint in the api spec, this method will only remove the token from the storage
     */
    logout(){
        this.currentUser = undefined;
        this.removeToken();
    }

    /**
     * returns a promise that will be resolved to a boolean. if token exists a request will be made to the server to check if the token is valid.
     * 
     * @returns {boolean} - if the user is logged in or not
     */
    is_logged_in(){
        if(!this.getToken()){
            return new Promise.resolve(false);
        }
        return this.getCurrentUser()
        .then((currentUser) => {
            let loggedIn = this.getToken() && currentUser
            return loggedIn;
        })
        .catch(() => {
            return false;
        });
    }
}

export default AC_Authentication;