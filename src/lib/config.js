const config = {
    apiUrl: 'https://conduit.productionready.io/api/',
    endpoints: {
        'login': 'users/login',
        'register': 'users',
        'blogSingleEndpoint': 'articles/:slug',
        'blogEndpoint': 'articles',
        'addComment': 'articles/:slug/comments',
        'getComments': 'articles/:slug/comments'
    }
};

export default config;