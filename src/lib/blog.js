import RealWorldApi from "./api";
import config from './config';
import queryString from 'query-string';
import AC_Authentication from "./authentication";

/**
 * Class to do tasks related to blog. e.g. get single article, get multiple articles, etc. users RealWorlApi class.
 */
class AC_Blog {
    constructor(){
        let { apiUrl, endpoints } = config;
        this.api = new RealWorldApi(apiUrl);
        this.authentication = new AC_Authentication();
        if(endpoints){
            this.blogSingleEndpoint = endpoints.blogSingleEndpoint;
            this.blogEndpoint = endpoints.blogEndpoint;
            this.addCommentEndpoint = endpoints.addComment;
            this.getCommentsEndpoint = endpoints.getComments;
        }
    }

    /**
     * gets latest published articles
     * @param {*} args - An optional object which will override default request params.
     * {
     *  page: page number, defaults to 1,
     *  limit: articles per page, defaults to 20,
     *  tag: articles tag,
     *  author: articles author
     * }
     *
     */
    getArticles(args){
        args = args || {};

        let defaultLimit = 20;
        let { page, limit, tag, author } = args;
        
        page = page || 1;
        limit = limit || defaultLimit;

        let offset = (page - 1)*limit;
        let qsParams = {
            limit,
            offset
        };
        if(tag){
            qsParams['tag'] = tag;
        }
        if(author){
            qsParams['author'] = author;
        }

        let blogEndpoint = this.blogEndpoint;

        let token = this.authentication.getToken();
        let qs = queryString.stringify(qsParams);
        let path = `${blogEndpoint}?${qs}`;
        let articlesPromise = this.api.get(path, token).then((response) => {
            let status = response.status;
            return response.json().then((result) => {
                if(status == 200){
                    if(result){
                        result.perPage = limit;
                        return result;
                    }
                }
                throw response;
            })
        });
        return articlesPromise;
    }

    /**
     * Get article by slug
     * @param {string} slug - each article has a unique slug
     */
    getArticle(slug){
        let token = this.authentication.getToken();
        
        let blogSingleEndpoint = this.blogSingleEndpoint;
        blogSingleEndpoint = blogSingleEndpoint.replace(/:slug/, slug);
        
        let path = blogSingleEndpoint;
        let articlePromise = this.api.get(path, token).then((response) => {
            let status = response.status;
            return response.json().then((result) => {
                if(status == 200){
                    if(result && result.article){
                        return result.article;
                    }
                }
                throw response;
            });
        });
        return articlePromise;
    }

    /**
     * Add a new comment to an article. return a promise that resolves to the added comment on success;
     * @param {string} articleSlug - slug of the article that the comment will be added. 
     * @param {*} comment - a comment object: { comment: { body: 'comment body' } }
     */
    submitComment(articleSlug, comment){
        let token = this.authentication.getToken();

        let addCommentEndpoint = this.addCommentEndpoint;
        addCommentEndpoint = addCommentEndpoint.replace(/:slug/, articleSlug);

        let path = addCommentEndpoint;
        let addCommentPromise = this.api.post(path, { comment }, token).then((response) => {
            let status = response.status;
            if(status == 200){
                return response.json().then((result) => {
                    return result.comment;
                });
            } else {
                throw response;
            }
        });

        return addCommentPromise;
    }

    /**
     * Get comments of an article
     * @param {string} articleSlug - slug of article which it's comments will be returned.
     */
    getArticleComments(articleSlug){
        let token = this.authentication.getToken();

        let getCommentsEndpoint = this.getCommentsEndpoint;
        getCommentsEndpoint = getCommentsEndpoint.replace(/:slug/, articleSlug);

        let getCommentsPromise = this.api.get(getCommentsEndpoint, token).then((response) => {
            let status = response.status;
            if(status == 200){
                return response.json().then((result) => {
                    return result.comments;
                });
            } else {
                throw response;
            }
        });

        return getCommentsPromise;
    }
}

export default AC_Blog;