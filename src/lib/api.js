import { removeTrailingSlash } from '../utils/functions';

/**
 * Class to connect to RealWorld api
 */
class RealWorldApi {
    /**
     * 
     * @param {string} apiUrl - base api url. example: "https://conduit.productionready.io/api/" 
     */
    constructor(apiUrl){
        this.apiUrl = removeTrailingSlash(apiUrl);
    }

    /**
     * sends a get request to the api endpoint
     * 
     * @param {string} endpoint - get request endpoint - e.g. "articles", "articles/hello-qwq8x4", ...
     * if the endpoint takes some query string params, this query string should be included in the 'endpoint' parameter.
     * 
     * @param {string} token - if the endpoint needs authentication this jwtToken should be provided. this token will be put in the
     * request's "Authorization" header. 
     */
    get(endpoint, token){
        let jwtToken = token;
        let url = `${this.apiUrl}/${endpoint}`;
        let headers = {
            "Content-Type": "application/json"
        };
        if(jwtToken){
            headers["Authorization"] = `Token ${jwtToken}`;
        }
        let args = {
            method: 'GET',
            headers: headers
        };
        let getPromise = fetch(url, args);
        return getPromise;
    }

    /**
     * sends a post request to the given endpoint
     * 
     * @param {string} endpoint - post request endpoint - e.g. "articles", ...
     * @param {*} data - data that will be sent along with the post request. data will be passed to the JSON.stringify function and is converted to a
     * json string. all request content-type headers are set to 'application/json'
     * @param {string} token -  if the endpoint needs authentication this jwtToken should be provided. this token will be put in the
     * request's "Authorization" header. 
     */
    post(endpoint, data, token){
        let jwtToken = token;
        let url = `${this.apiUrl}/${endpoint}`;
        let headers = {
            "Content-Type": "application/json"
        };
        if(jwtToken){
            headers["Authorization"] = `Token ${jwtToken}`;
        }
        let args = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        };
        let postPromise = fetch(url, args);
        return postPromise;
    }

    /**
     * sends a put request to the given endpoint
     * 
     * @param {string} endpoint - put request endpoint - e.g. "articles", ...
     * @param {*} data - data that will be sent along with the put request. data will be passed to the JSON.stringify function and is converted to a
     * json string. all request content-type headers are set to 'application/json'
     * @param {string} token -  if the endpoint needs authentication this jwtToken should be provided. this token will be put in the
     * request's "Authorization" header.
     */
    put(endpoint, data, token){
        let jwtToken = token;
        let url = `${this.apiUrl}/${endpoint}`;
        let headers = {
            "Content-Type": "application/json"
        };
        if(jwtToken){
            headers["Authorization"] = `Token ${jwtToken}`;
        }
        let args = {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(data),
        };
        let putPromise = fetch(url, args);
        return putPromise;
    }

    /**
     * sends a delete request to the given endpoint
     * 
     * @param {string} endpoint - delete request endpoint - e.g. "articles", ...
     * @param {string} token -  if the endpoint needs authentication this jwtToken should be provided. this token will be put in the
     * request's "Authorization" header.
     */
    delete(endpoint, token){
        let jwtToken = token;
        let url = `${this.apiUrl}/${endpoint}`;
        let headers = {
            "Content-Type": "application/json"
        };
        if(jwtToken){
            headers["Authorization"] = `Token ${jwtToken}`;
        }
        let args = {
            method: 'DELETE',
            headers: headers
        };
        let deletePromise = fetch(url, args);
        return deletePromise;
    }
}

export default RealWorldApi;